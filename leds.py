# import apa102 library
import apa102

# define how many LEDs there are
num_led = 59

# setup strip
strip = apa102.APA102(num_led=num_led, global_brightness=100, order='rbg')

# get first four LEDs to go white and next 4 to go red
for led in range(0, 6):
    strip.set_pixel_rgb(led,0xFFFFFF,100) # Paint 100% right headlight
for led in range(6, 24):
    strip.set_pixel_rgb(led,0x33FF33,5) # Paint 5% blue for middle bit
for led in range(23, 29):
    strip.set_pixel_rgb(led,0xFFFFFF,100) # Paint 100% for left headlight
for led in range(30, 36):
    strip.set_pixel_rgb(led,0xFF0000,100) # Paint 100% red for left tail light
for led in range(36, 53):
    strip.set_pixel_rgb(led,0xFF0000,5) # Paint 5% red for tail light middle
for led in range(53, 59):
    strip.set_pixel_rgb(led,0xFF0000,100) # Paint 100% for right tail light

# apply changes
strip.show()
