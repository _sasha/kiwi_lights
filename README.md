# Kiwibot LEDs

Very basic script that sets the 4 LEDs in the front to white and 4 LEDs in the back to red. The LEDs are APA102 driven over SPI.

## Setup
Enable SPI with raspi-config

You need to run `./setup.sh`

Wiring is very simple:

![APA102 wiring](http://www.elec-tron.org/wp-content/uploads/2016/03/rpitoapa102-1024x542.jpg)


## Usage
On reboot the Pi will power on the LEDs. This behaviour can be changed with `sudo crontab -e` and removing the line with `@reboot pyhton /home/pi/kiwi_lights/leds.py`.