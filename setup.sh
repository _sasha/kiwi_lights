#!/bin/bash

# install spidev
sudo apt-get update
sudo apt-get install python3-pip
sudo pip3 install spidev

# add to crontab
(sudo crontab -l 2>/dev/null; echo "@reboot /usr/bin/python3 /home/pi/kiwi_leds/leds.py &") | sudo crontab -

# start it to test
/usr/bin/python3 /home/pi/kiwi_leds/leds.py &